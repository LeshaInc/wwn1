const { stylus } = require('svelte-preprocess');
module.exports = {
  preprocess: [stylus()],
};
