use serde::{Deserialize, Serialize};

use crate::map::MapTemplateId;

/// Lobby settings
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LobbySettings {
    /// Lobby visible name
    pub name: String,
    /// Is lobby publicly listed
    pub is_public: bool,
    /// Password
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    /// Map template
    pub template_id: MapTemplateId,
    /// Max human players
    pub max_human_players: u8,
}

/// Lobby
pub struct Lobby {
    settings: LobbySettings,
}

impl Lobby {
    /// Create a new lobby
    pub fn new(settings: LobbySettings) -> Lobby {
        Lobby { settings }
    }

    /// Get lobby settings
    pub fn settings(&self) -> &LobbySettings {
        &self.settings
    }
}
