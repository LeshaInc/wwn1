import { readable } from "svelte/store";

let socket;
let handlers = {}
let connectHandlers = []

export function addHandler(type, fun) {
  if (!handlers.hasOwnProperty(type)) {
    handlers[type] = []
  }

  const id = handlers[type].length;
  handlers[type].push(fun);
  return { id: id, type: type };
}

export function removeHandler(id) {
  handlers[id.type].splice(id.id, 1);
}

export function addConnectHandler(fun) {
  const id = connectHandlers.length;
  connectHandlers.push(fun);
  return id;
}

export function removeConnectHandler(id) {
  connectHandlers.splice(id, 1)
}

export function send(msg) {
  socket.send(JSON.stringify(msg));
}

export function connect() {
  socket = new WebSocket("ws://0.0.0.0:8000");
  setupListeners();
}

function setupListeners() {
  socket.onmessage = (e) => {
    const msg = JSON.parse(e.data);
    if (handlers.hasOwnProperty(msg.type)) {
      for (const fun of handlers[msg.type]) {
        fun(msg)
      }
    }
  };

  socket.onopen = () => {
    for (const fun of connectHandlers) {
      fun()
    }
  };

  socket.onclose = (e) => {
    console.log("websocket closed");
    setTimeout(connect, 1000);
  };

  socket.onerror = (e) => {
    console.error(e);
    socket.close();
  }
}

export const numClients = readable(1, (set) => {
  const id = addHandler("notificationNumClients", (msg) => {
    set(msg.numClients);
  });

  () => removeHandler(id)
});

export const mapTemplates = readable({}, (set) => {
  const a = addConnectHandler(() => {
    send({type: "reqMapList"});
  });

  const b = addHandler("resMapList", (msg) => {
    set(msg.maps);
  });

  () => {
    removeConnectHandler(a);
    removeHandler(b);
  }
});
