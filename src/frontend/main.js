import App from "./App.svelte";
import { connect } from "./websocket.js";

connect();

const app = new App({
  target: document.body
});

export default app;
