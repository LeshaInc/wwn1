use std::sync::Arc;

use anyhow::{bail, Context, Error, Result};
use futures_util::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use tokio_tungstenite::tungstenite::Message as WsMessage;
use tokio_tungstenite::WebSocketStream;
use tracing::{error, trace};

use crate::error::ErrorKind;
use crate::protocol::{self, *};
use crate::server::ServerContext;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ClientId(pub usize);

pub struct Client {
    id: ClientId,
    stream: WebSocketStream<TcpStream>,
    rx: UnboundedReceiver<Message>,
    server_ctx: Arc<ServerContext>,
    state: ClientState,
}

pub struct ClientContext {
    pub tx: UnboundedSender<Message>,
}

impl Client {
    pub async fn new(stream: WebSocketStream<TcpStream>, server_ctx: Arc<ServerContext>) -> Client {
        let (tx, rx) = mpsc::unbounded_channel();
        let ctx = Arc::new(ClientContext { tx });
        let id = server_ctx.add_client(ctx).await;
        Client {
            id,
            stream,
            rx,
            server_ctx,
            state: ClientState::Menu,
        }
    }

    pub async fn send(&mut self, msg: impl Into<Message>) -> Result<()> {
        let msg = msg.into();
        let text = serde_json::to_string(&msg).context("Serialization error")?;
        self.send_text(text).await
    }

    pub async fn send_text(&mut self, text: impl Into<String>) -> Result<()> {
        self.stream
            .send(WsMessage::Text(text.into()))
            .await
            .context("Cannot send message")?;
        Ok(())
    }

    pub async fn send_error(&mut self, err: Error) -> Result<()> {
        let kind = match err.downcast_ref::<ErrorKind>() {
            Some(v) => *v,
            None => ErrorKind::InternalError,
        };

        let cause = err.chain().skip(1).map(|v| v.to_string()).collect();
        self.send(protocol::Error {
            kind,
            message: err.to_string(),
            cause,
            backtrace: Vec::new(),
        })
        .await?;

        Ok(())
    }

    pub async fn handle(&mut self) -> Result<()> {
        loop {
            tokio::select! {
                msg = self.stream.next() => match msg {
                    Some(Ok(msg)) => {
                        if let Err(e) = self.handle_ws_msg(msg).await {
                            error!("{:?}", e);
                            self.send_error(e).await?;
                        }
                    }
                    Some(Err(e)) => return Err(Error::new(e).context("Cannot receive message")),
                    None => break,
                },
                msg = self.rx.recv() => if let Some(msg) = msg {
                    self.send(msg).await?;
                }
            }
        }

        Ok(())
    }

    async fn handle_ws_msg(&mut self, ws_msg: WsMessage) -> Result<()> {
        trace!(?ws_msg);

        match ws_msg {
            WsMessage::Text(text) => {
                let msg = serde_json::from_str(&text).context(ErrorKind::ParseError)?;
                self.handle_msg(msg).await?;
            }

            WsMessage::Ping(data) => self
                .stream
                .send(WsMessage::Pong(data))
                .await
                .context("Cannot reply to ping")?,

            _ => (),
        }

        Ok(())
    }

    async fn handle_msg(&mut self, msg: Message) -> Result<()> {
        match (msg, &mut self.state) {
            (Message::Common(msg), _) => self.handle_common_msg(msg).await,
            (Message::Menu(msg), ClientState::Menu) => self.handle_menu_msg(msg).await,
            _ => bail!(ErrorKind::NotAllowed),
        }
    }

    async fn handle_common_msg(&mut self, msg: CommonMessage) -> Result<()> {
        match msg {
            _ => bail!(ErrorKind::NotAllowed),
        }
    }

    async fn handle_menu_msg(&mut self, msg: MenuMessage) -> Result<()> {
        match msg {
            MenuMessage::ReqMapList(_) => {
                self.send_text(self.server_ctx.list_maps_message.to_owned())
                    .await
            }
            _ => bail!(ErrorKind::NotAllowed),
        }
    }

    pub async fn destroy(self) {
        self.server_ctx.remove_client(self.id).await;
    }
}

enum ClientState {
    Menu,
}
