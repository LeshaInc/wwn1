use thiserror::Error;

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Error)]
pub enum ErrorKind {
    #[error("internal server error")]
    InternalError,
    #[error("bad packet (parse error)")]
    ParseError,
    #[error("message not allowed in current context")]
    NotAllowed,
}
