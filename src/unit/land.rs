//! Land units and battles

use rand::Rng;

/// Base stats of a land unit
#[derive(Clone, Copy, Debug)]
pub struct LandUnitStats {
    /// Unit strength (equipment + manpower)
    pub strength: f32,
    /// Unit organization (morale, discipline, exhaustion, satiety, etc)
    pub organization: f32,
    /// Number of times the defending unit can defend in a round
    pub defense: f32,
    /// Number of times the attacking unit can defend in a round
    pub toughness: f32,
    /// Number of times the unit can attack a soft target in a round
    pub soft_attack: f32,
    /// Number of times the unit can attack a hard target in a round
    pub hard_attack: f32,
    /// How soft is the unit (between 0 and 1)
    pub softness: f32,
    /// How strong is the armor of the unit
    pub armor: f32,
    /// How well does the unit pierce enemy armor
    pub piercing: f32,
}

/// Current unit state
#[derive(Clone, Copy, Debug)]
pub struct LandUnitProps {
    /// Current strength of the unit
    pub strength: f32,
    /// Current organization of the unit
    pub organization: f32,
}

/// Land battle modifiers
#[derive(Clone, Copy, Debug)]
pub struct LandModifiers {
    /// Soft attack multiplier
    pub soft_attack: f32,
    /// Hard attack multiplier
    pub hard_attack: f32,
    /// Defense multiplier
    pub defense: f32,
}

/// Damage to a land unit
#[derive(Default)]
pub struct LandDamage {
    /// Strength damage
    pub strength: f32,
    /// Organization damage
    pub organization: f32,
}

fn gen_bool<R: Rng>(rng: &mut R, p: f32) -> bool {
    if p - 1.0 < f32::EPSILON {
        true
    } else {
        rng.gen::<u32>() as f32 >= p * (u32::max_value() as f32)
    }
}

fn round<R: Rng>(rng: &mut R, v: f32) -> i32 {
    let addone = gen_bool(rng, v.trunc());
    v.floor() as i32 + addone as i32
}

/// Calculate damage to the attacker and the defender after a battle round
pub fn land_battle_round<R: Rng>(
    att_stats: &LandUnitStats,
    def_stats: &LandUnitStats,
    modifiers: &LandModifiers,
    rng: &mut R,
) -> (LandDamage, LandDamage) {
    let att_soft = gen_bool(rng, def_stats.softness);
    let def_soft = gen_bool(rng, att_stats.softness);

    let att_sp = if att_soft {
        modifiers.soft_attack * att_stats.soft_attack
    } else {
        modifiers.hard_attack * att_stats.hard_attack
    };

    let def_sp = if def_soft {
        modifiers.soft_attack * def_stats.soft_attack
    } else {
        modifiers.hard_attack * def_stats.hard_attack
    };

    // Shot points
    let mut att_sp = round(rng, att_sp);
    let mut def_sp = round(rng, def_sp);

    // Defense points
    let mut att_dp = round(rng, att_stats.toughness * modifiers.defense);
    let mut def_dp = round(rng, def_stats.defense * modifiers.defense);

    // Received damage
    let mut att_dmg = LandDamage::default();
    let mut def_dmg = LandDamage::default();

    // Do attacks pierce armor?
    let att_ap = att_stats.piercing >= def_stats.armor;
    let def_ap = def_stats.piercing >= att_stats.armor;

    let mut att_turn = true;

    while att_sp > 0 || def_sp > 0 {
        let mut side_a = (&mut att_sp, &mut att_dp, &mut att_dmg, att_ap, att_soft);
        let mut side_b = (&mut def_sp, &mut def_dp, &mut def_dmg, def_ap, def_soft);
        if !att_turn {
            std::mem::swap(&mut side_a, &mut side_b);
        }

        // A shoots B
        let (a_sp, _, a_dmg, a_ap, a_soft) = side_a;
        let (_, b_dp, b_dmg, _, _) = side_b;

        if *a_sp > 0 {
            *a_sp -= 1;

            let hit_p = if *b_dp > 0 {
                *b_dp -= 1;
                0.3
            } else {
                0.52
            };

            let mut hit = gen_bool(rng, hit_p) as i32 as f32;

            if !a_soft && !a_ap {
                if hit > 0.0 {
                    hit *= rng.gen_range(0.0, 0.7);
                }

                a_dmg.organization += rng.gen_range(0.0, 0.1);
            }

            b_dmg.strength += hit * 2.25;
            b_dmg.organization += hit * 0.2;
        }

        att_turn = !att_turn;
    }

    (att_dmg, def_dmg)
}
