use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::error::ErrorKind;

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
#[serde(rename_all = "camelCase")]
pub enum Message {
    Common(CommonMessage),
    Menu(MenuMessage),
}

macro_rules! define_messages {
    (
        $( #[$uattr:meta] )*
        pub enum $name:ident as $mname:ident;

        $(
            $( #[$mattr:meta] )*
            pub struct $msg:ident $fields:tt
        )+
    ) => {

        $( #[$uattr] )*
        #[derive(Clone, Debug, Serialize, Deserialize)]
        #[serde(tag = "type")]
        #[serde(rename_all = "camelCase")]
        pub enum $name {
            $($msg($msg),)+
        }

        impl From<$name> for Message {
            fn from(v: $name) -> Message {
                Message::$mname(v)
            }
        }

        $(
            impl From<$msg> for $name {
                fn from(v: $msg) -> $name {
                    $name::$msg(v)
                }
            }

            impl From<$msg> for Message {
                fn from(v: $msg) -> Message {
                    Message::$mname($name::$msg(v))
                }
            }
        )+

        $(
            #[derive(Clone, Debug, Serialize, Deserialize)]
            #[serde(rename_all = "camelCase")]
            $( #[$mattr] )*
            pub struct $msg $fields
        )+
    };
}

define_messages! {
    /// Messages valid in all contexts
    pub enum CommonMessage as Common;

    /// Message sent to client to indicate an error
    pub struct Error {
        pub kind: ErrorKind,
        pub message: String,
        pub cause: Vec<String>,
        #[serde(default)]
        #[serde(skip_serializing_if = "Vec::is_empty")]
        pub backtrace: Vec<String>,
    }

    /// Notification sent to client to inform that the number of clients online has changed
    pub struct NotificationNumClients {
        pub num_clients: usize,
    }
}

define_messages! {
    /// Messages valid only in menu
    pub enum MenuMessage as Menu;

    /// Request map templates
    pub struct ReqMapList;

    pub struct ResMapList {
        pub maps: Vec<MapEntry>,
    }

    /// Request the list of public lobbies
    pub struct ReqPublicLobbies;

    /// Response with the list of public lobbies
    pub struct ResPublicLobbies {
        pub lobbies: Vec<PublicLobbyEntry>,
    }

    /// Request to create a lobby
    pub struct ReqCreateLobby {
        pub name: String,
        pub is_public: bool,
        #[serde(default)]
        pub password: Option<String>,
        pub map_id: u32,
        pub human_players: u8,
    }

    /// Lobby created successfully. The client joined it automatically
    pub struct ResCreateLobby {
        pub uuid: Uuid,
    }

    /// Request to join a lobby
    pub struct ReqJoinLobby {
        pub uuid: Uuid,
        #[serde(default)]
        pub password: Option<String>,
    }

    /// Successfully joined the lobby
    pub struct ResJoinLobby;
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PublicLobbyEntry {
    pub name: String,
    pub uuid: Uuid,
    pub cur_players: u8,
    pub max_players: u8,
    pub has_password: bool,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MapEntry {
    pub name: String,
    pub num_countries: u8,
    pub svg: String,
}
