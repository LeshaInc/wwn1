//! Generic map storage

use std::ops::{Index, IndexMut};

use nalgebra::Point2;

/// Doubly-connected edge list (aka half-edge list) used to store a polygonal grid
#[derive(Clone, Debug)]
pub struct DCEL<F, E, V> {
    half_edges: Vec<HalfEdge>,
    faces: Vec<Face<F>>,
    edges: Vec<Edge<E>>,
    vertices: Vec<Vertex<V>>,
}

const NONE: u32 = u32::max_value();

macro_rules! impl_index {
    ($name:ident) => {
        impl<T> Index<$name> for Vec<T> {
            type Output = T;

            fn index(&self, index: $name) -> &T {
                &self[index.0 as usize]
            }
        }

        impl<T> IndexMut<$name> for Vec<T> {
            fn index_mut(&mut self, index: $name) -> &mut T {
                &mut self[index.0 as usize]
            }
        }
    };
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct HalfEdgeId(u32);
impl_index!(HalfEdgeId);

#[derive(Clone, Copy, Debug)]
struct HalfEdge {
    opposite: HalfEdgeId,
    next: HalfEdgeId,
    prev: HalfEdgeId,
    face: FaceId,
    edge: EdgeId,
    vertex: VertexId,
}

/// DCEL face ID
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct FaceId(u32);
impl_index!(FaceId);

#[derive(Clone, Debug)]
struct Face<F> {
    data: F,
    half_edge: HalfEdgeId,
}

/// DCEL edge ID
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct EdgeId(u32);
impl_index!(EdgeId);

#[derive(Clone, Debug)]
struct Edge<E> {
    data: E,
    half_edge: HalfEdgeId,
}

/// DCEL vertex ID
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct VertexId(u32);
impl_index!(VertexId);

#[derive(Clone, Debug)]
struct Vertex<V> {
    data: V,
    half_edge: HalfEdgeId,
}

impl<F, E, V> Default for DCEL<F, E, V> {
    fn default() -> Self {
        DCEL::new()
    }
}

impl<F, E, V> DCEL<F, E, V> {
    /// Create an empty DCEL
    pub fn new() -> DCEL<F, E, V> {
        DCEL {
            half_edges: Vec::new(),
            faces: Vec::new(),
            edges: Vec::new(),
            vertices: Vec::new(),
        }
    }

    /// Add a vertex with specified data
    pub fn add_vertex(&mut self, data: V) -> VertexId {
        let id = VertexId(self.vertices.len() as u32);

        self.vertices.push(Vertex {
            data,
            half_edge: HalfEdgeId(NONE),
        });

        id
    }

    /// Get a shared reference to the vertex by ID
    pub fn vertex(&self, id: VertexId) -> &V {
        &self.vertices[id].data
    }

    /// Get a mutable reference to the vertex by ID
    pub fn vertex_mut(&mut self, id: VertexId) -> &mut V {
        &mut self.vertices[id].data
    }

    /// Get an immutable iterator over all vertices
    pub fn vertices(&self) -> impl Iterator<Item = (VertexId, &V)> + '_ {
        let it = self.vertices.iter().enumerate();
        it.map(|(id, v)| (VertexId(id as u32), &v.data))
    }

    /// Get a mutable iterator over all vertices
    pub fn vertices_mut(&mut self) -> impl Iterator<Item = (VertexId, &mut V)> + '_ {
        let it = self.vertices.iter_mut().enumerate();
        it.map(|(id, v)| (VertexId(id as u32), &mut v.data))
    }

    /// Map every vertex from V to V1
    pub fn map_vertices<V1, M: FnMut(VertexId, V) -> V1>(self, mut f: M) -> DCEL<F, E, V1> {
        let vertices = self.vertices.into_iter().enumerate();
        DCEL {
            half_edges: self.half_edges,
            faces: self.faces,
            edges: self.edges,
            vertices: vertices
                .map(|(id, v)| Vertex {
                    data: f(VertexId(id as u32), v.data),
                    half_edge: v.half_edge,
                })
                .collect(),
        }
    }

    /// Add an edge with specified data. To set edge vertices, use `connect_vertices`
    pub fn add_edge(&mut self, data: E) -> EdgeId {
        let id = EdgeId(self.edges.len() as u32);

        self.edges.push(Edge {
            data,
            half_edge: HalfEdgeId(NONE),
        });

        id
    }

    /// Get a shared reference to the edge by ID
    pub fn edge(&self, id: EdgeId) -> &E {
        &self.edges[id].data
    }

    /// Get a mutable reference to the edge by ID
    pub fn edge_mut(&mut self, id: EdgeId) -> &mut E {
        &mut self.edges[id].data
    }

    /// Get an immutable iterator over all edges
    pub fn edges(&self) -> impl Iterator<Item = (EdgeId, &E)> + '_ {
        let it = self.edges.iter().enumerate();
        it.map(|(id, v)| (EdgeId(id as u32), &v.data))
    }

    /// Get a mutable iterator over all edges
    pub fn edges_mut(&mut self) -> impl Iterator<Item = (EdgeId, &mut E)> + '_ {
        let it = self.edges.iter_mut().enumerate();
        it.map(|(id, v)| (EdgeId(id as u32), &mut v.data))
    }

    /// Map every edge from E to E1
    pub fn map_edges<E1, M: FnMut(EdgeId, E) -> E1>(self, mut f: M) -> DCEL<F, E1, V> {
        let edges = self.edges.into_iter().enumerate();
        DCEL {
            half_edges: self.half_edges,
            faces: self.faces,
            edges: edges
                .map(|(id, e)| Edge {
                    data: f(EdgeId(id as u32), e.data),
                    half_edge: e.half_edge,
                })
                .collect(),
            vertices: self.vertices,
        }
    }

    /// Add a face with specified data. To set face edges, use `connect_edges`
    pub fn add_face(&mut self, data: F) -> FaceId {
        let id = FaceId(self.faces.len() as u32);

        self.faces.push(Face {
            data,
            half_edge: HalfEdgeId(NONE),
        });

        id
    }

    /// Get a shared reference to the face by ID
    pub fn face(&self, id: FaceId) -> &F {
        &self.faces[id].data
    }

    /// Get a mutable reference to the face by ID
    pub fn face_mut(&mut self, id: FaceId) -> &mut F {
        &mut self.faces[id].data
    }

    /// Get an immutable iterator over all faces
    pub fn faces(&self) -> impl Iterator<Item = (FaceId, &F)> + '_ {
        let it = self.faces.iter().enumerate();
        it.map(|(id, v)| (FaceId(id as u32), &v.data))
    }

    /// Get an mutable iterator over all faces
    pub fn faces_mut(&mut self) -> impl Iterator<Item = (FaceId, &mut F)> + '_ {
        let it = self.faces.iter_mut().enumerate();
        it.map(|(id, v)| (FaceId(id as u32), &mut v.data))
    }

    /// Map every face from F to F1
    pub fn map_faces<F1, M: FnMut(FaceId, F) -> F1>(self, mut f: M) -> DCEL<F1, E, V> {
        let faces = self.faces.into_iter().enumerate();
        DCEL {
            half_edges: self.half_edges,
            faces: faces
                .map(|(id, fa)| Face {
                    data: f(FaceId(id as u32), fa.data),
                    half_edge: fa.half_edge,
                })
                .collect(),
            edges: self.edges,
            vertices: self.vertices,
        }
    }

    /// Set edge vertices
    pub fn connect_vertices(&mut self, a_id: VertexId, b_id: VertexId, e_id: EdgeId) {
        if self.edges[e_id].half_edge.0 != NONE {
            return;
        }

        let h1_id = HalfEdgeId(self.half_edges.len() as u32);
        let h2_id = HalfEdgeId(h1_id.0 + 1);

        self.vertices[a_id].half_edge = h1_id;
        self.vertices[b_id].half_edge = h2_id;

        self.edges[e_id].half_edge = h1_id;

        let h1 = HalfEdge {
            opposite: h2_id,
            next: HalfEdgeId(NONE),
            prev: HalfEdgeId(NONE),
            face: FaceId(NONE),
            edge: e_id,
            vertex: a_id,
        };

        let h2 = HalfEdge {
            opposite: h1_id,
            vertex: b_id,
            ..h1
        };

        self.half_edges.push(h1);
        self.half_edges.push(h2);
    }

    /// Set face edges
    pub fn connect_edges(&mut self, a_id: EdgeId, b_id: EdgeId, f_id: FaceId) {
        let h1_id = self.edges[a_id].half_edge;
        let h2_id = self.edges[b_id].half_edge;

        let h3_id = self.half_edges[h1_id].opposite;
        let h4_id = self.half_edges[h2_id].opposite;

        let v1_id = self.half_edges[h1_id].vertex;
        let v2_id = self.half_edges[h2_id].vertex;
        let v3_id = self.half_edges[h3_id].vertex;
        let v4_id = self.half_edges[h4_id].vertex;

        if v3_id == v2_id {
            self.half_edges[h1_id].next = h2_id;
            self.half_edges[h2_id].prev = h1_id;

            self.half_edges[h1_id].face = f_id;
            self.half_edges[h2_id].face = f_id;
            self.faces[f_id].half_edge = h1_id;
        } else if v3_id == v4_id {
            self.half_edges[h1_id].next = h4_id;
            self.half_edges[h4_id].prev = h1_id;

            self.half_edges[h1_id].face = f_id;
            self.half_edges[h4_id].face = f_id;
            self.faces[f_id].half_edge = h4_id;
        } else if v1_id == v2_id {
            self.half_edges[h3_id].next = h2_id;
            self.half_edges[h2_id].prev = h3_id;

            self.half_edges[h3_id].face = f_id;
            self.half_edges[h2_id].face = f_id;
            self.faces[f_id].half_edge = h2_id;
        } else if v1_id == v4_id {
            self.half_edges[h3_id].next = h4_id;
            self.half_edges[h4_id].prev = h3_id;

            self.half_edges[h3_id].face = f_id;
            self.half_edges[h4_id].face = f_id;
            self.faces[f_id].half_edge = h3_id;
        } else {
            panic!("can't connect edges");
        }
    }

    fn face_halfedges(&self, face: FaceId) -> impl Iterator<Item = HalfEdgeId> + '_ {
        let start = self.faces[face].half_edge;
        let mut cur = start;
        let mut finished = false;
        std::iter::from_fn(move || {
            if finished {
                return None;
            }

            let prev = cur;
            cur = self.half_edges[cur].next;
            finished = cur == start;
            Some(prev)
        })
    }

    /// Iterate through vertices of the face
    pub fn face_vertices(&self, face: FaceId) -> impl Iterator<Item = VertexId> + '_ {
        self.face_halfedges(face)
            .map(move |id| self.half_edges[self.half_edges[id].opposite].vertex)
    }

    /// Iterate through edges of the face
    pub fn face_edges(&self, face: FaceId) -> impl Iterator<Item = EdgeId> + '_ {
        self.face_halfedges(face)
            .map(move |id| self.half_edges[self.half_edges[id].opposite].edge)
    }

    /// Iterate through neighboring faces of the face
    pub fn face_neighbors(&self, face: FaceId) -> impl Iterator<Item = FaceId> + '_ {
        self.face_halfedges(face)
            .map(move |id| self.half_edges[self.half_edges[id].opposite].face)
    }

    /// Iterate through adjacent faces of the edge (no more than 2)
    pub fn edge_faces(&self, edge: EdgeId) -> impl Iterator<Item = FaceId> + '_ {
        let h1 = self.edges[edge].half_edge;
        let h2 = self.half_edges[h1].opposite;
        let a = Some(self.half_edges[h1].face).filter(|x| x.0 != NONE);
        let b = Some(self.half_edges[h2].face).filter(|x| x.0 != NONE);
        a.into_iter().chain(b)
    }

    /// Get edge vertices
    pub fn edge_vertices(&self, edge: EdgeId) -> [VertexId; 2] {
        let h = self.edges[edge].half_edge;
        let a = self.half_edges[h].vertex;
        let b = self.half_edges[self.half_edges[h].opposite].vertex;
        [a, b]
    }
}

impl<F, E> DCEL<F, E, Point2<f32>> {
    /// Get face center
    pub fn face_center(&self, face: FaceId) -> Point2<f32> {
        let (n_pts, sum) = self
            .face_vertices(face)
            .fold((0.0, Point2::new(0.0, 0.0)), |(n, sum), v| {
                (n + 1.0, sum + self.vertex(v).coords)
            });
        Point2::new(sum.x / n_pts, sum.y / n_pts)
    }
}
