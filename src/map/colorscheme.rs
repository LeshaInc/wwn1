use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

use anyhow::{Context, Result};

/// Map color scheme
pub struct ColorScheme {
    colors: Vec<[u8; 3]>,
}

impl ColorScheme {
    /// Open a color scheme
    pub fn open<P: AsRef<Path>>(path: P) -> Result<ColorScheme> {
        let mut colors = Vec::new();

        let path = path.as_ref();
        let file = File::open(path).with_context(|| format!("Cannot open {}", path.display()))?;
        let reader = BufReader::new(file);

        for line in reader.lines() {
            let line = line.context("Cannot read line")?;

            if line.len() != 6 {
                continue;
            }

            let hex = u32::from_str_radix(&line, 16).context("Cannot parse color")?;
            let red = hex >> 16;
            let green = (hex >> 8) & 0xFF;
            let blue = hex & 0xFF;
            colors.push([red as u8, green as u8, blue as u8]);
        }

        Ok(ColorScheme { colors })
    }

    /// Get a color
    pub fn get(&self, id: u8) -> [u8; 3] {
        self.colors[id as usize]
    }

    /// Get a color in css format
    pub fn get_css(&self, id: u8) -> String {
        let [r, g, b] = self.get(id);
        format!("rgb({}, {}, {})", r, g, b)
    }
}
