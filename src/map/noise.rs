//! Noisy edges

use std::collections::HashMap;

use nalgebra::Point2;
use rand::Rng;

use super::{EdgeId, FaceId, DCEL};

/// Shapes with defined center
pub trait HasCenter {
    /// Get barycenter of the shape
    fn center(&self) -> Point2<f32>;
}

/// Generate noisy edge
pub fn noisy_edge<F: HasCenter, E, R: Rng>(
    dcel: &DCEL<F, E, Point2<f32>>,
    edge: EdgeId,
    steps: u8,
    mut factor: f32,
    rng: &mut R,
) -> Vec<Point2<f32>> {
    let steps = u32::from(steps);

    let [start, end] = dcel.edge_vertices(edge);
    let [start, end] = [*dcel.vertex(start), *dcel.vertex(end)];

    // TODO: store face centers in map

    let mut faces = dcel.edge_faces(edge);
    let a_center = match faces.next() {
        Some(v) => dcel.face(v).center(),
        None => return vec![start, end],
    };

    let b_center = match faces.next() {
        Some(v) => dcel.face(v).center(),
        None => {
            let mid = Point2::from((start.coords + end.coords) * 0.5);
            mid + (mid - a_center)
        }
    };

    let two = 2usize;
    let num_points = two.pow(steps) + 1;
    let mut points = vec![start; num_points];
    points[num_points - 1] = end;

    for step in 0..steps {
        let size = two.pow(steps - step);
        for idx in 0..two.pow(step) {
            let i_left = idx * size;
            let i_mid = idx * size + size / 2;
            let i_right = (idx + 1) * size;
            let p_left = points[i_left];
            let p_right = points[i_right];

            let mut p_mid = Point2::from((p_left.coords + p_right.coords) * 0.5);
            let a_dir = a_center - p_mid;
            let b_dir = b_center - p_mid;
            let dist = rng.gen_range(-factor, factor);
            p_mid -= a_dir * dist.min(0.0) - b_dir * dist.max(0.0);
            points[i_mid] = p_mid;
        }

        factor *= 0.5;
    }

    points
}

/// Generate noisy edges for the whole map
pub fn noisy_edges<F: HasCenter, E, R: Rng>(
    dcel: &DCEL<F, E, Point2<f32>>,
    steps: u8,
    factor: f32,
    rng: &mut R,
) -> HashMap<EdgeId, Vec<Point2<f32>>> {
    dcel.edges()
        .map(|(edge_id, _)| (edge_id, noisy_edge(dcel, edge_id, steps, factor, rng)))
        .collect()
}

/// Get points of a noisy face
pub fn face_noisy_points<F, E>(
    dcel: &DCEL<F, E, Point2<f32>>,
    edges: &HashMap<EdgeId, Vec<Point2<f32>>>,
    face_id: FaceId,
) -> Vec<Point2<f32>> {
    let mut points = Vec::new();

    for edge_id in dcel.face_edges(face_id) {
        let iter = edges[&edge_id].iter();
        let is_rev = dcel.edge_faces(edge_id).next() != Some(face_id);
        if is_rev {
            points.extend(iter.rev().skip(1));
        } else {
            points.extend(iter.skip(1));
        }
    }

    points
}
