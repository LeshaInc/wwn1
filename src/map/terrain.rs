use rand::Rng;
use svg::node::element::{Definitions, Path};

/// Terrain types
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Terrain {
    /// Water (sea or ocean)
    Water,
    /// Plains
    Plains,
    /// Hills
    Hills,
    /// Forest
    Forest,
    /// Mountains
    Mountains,
    /// Urban (cities)
    Urban,
}

/// Poisson-disc sampled pattern
#[derive(Clone, Copy, Debug)]
pub struct SampledPattern {
    /// SVG definition ID
    pub name: &'static str,
    /// Ellipse X radius
    pub rx: f32,
    /// Ellipse Y radius
    pub ry: f32,
}

impl SampledPattern {
    pub fn new(name: &'static str, rx: f32, ry: f32) -> SampledPattern {
        SampledPattern { name, rx, ry }
    }
}

impl Terrain {
    pub fn random_weights() -> (&'static [i32], &'static [Terrain]) {
        use Terrain::*;
        let w = &[2, 3, 2, 1];
        let c = &[Plains, Forest, Hills, Mountains];
        (w, c)
    }

    pub fn svg_defs() -> Definitions {
        Definitions::new()
            .add(
                Path::new()
                    .set("id", "tree0")
                    .set("d", include_str!("../../assets/tree0.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.2)"),
            )
            .add(
                Path::new()
                    .set("id", "tree1")
                    .set("d", include_str!("../../assets/tree1.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.2)"),
            )
            .add(
                Path::new()
                    .set("id", "house")
                    .set("d", include_str!("../../assets/house.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.2)"),
            )
            .add(
                Path::new()
                    .set("id", "hill")
                    .set("d", include_str!("../../assets/hill.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.2)"),
            )
            .add(
                Path::new()
                    .set("id", "mountain")
                    .set("d", include_str!("../../assets/mountain.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.2)"),
            )
    }

    pub fn sampled_pattern<R: Rng>(self, rng: &mut R) -> Option<SampledPattern> {
        Some(match self {
            Terrain::Forest => {
                if rng.gen() {
                    SampledPattern::new("#tree0", 9.0, 16.0)
                } else {
                    SampledPattern::new("#tree1", 10.0, 17.0)
                }
            }
            Terrain::Urban => SampledPattern::new("#house", 16.0, 16.0),
            Terrain::Hills => SampledPattern::new("#hill", 24.0, 8.0),
            Terrain::Mountains => SampledPattern::new("#mountain", 26.0, 15.0),
            _ => return None,
        })
    }
}
