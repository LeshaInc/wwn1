//! Map template

use std::collections::HashSet;

use nalgebra::Point2;
use svg::Document;

use super::generation::{GenEdge, GenFace, GenMap};
use super::{noise, render, ColorScheme, Hex, HexMap, DCEL};

/// ID of the region
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct RegionId(pub u8);

impl RegionId {
    /// Water (ID 0)
    pub const WATER: RegionId = RegionId(0);

    /// Is it water
    pub fn is_water(self) -> bool {
        self == RegionId::WATER
    }
}

/// FaceTemplate
#[derive(Clone, Debug)]
pub struct FaceTemplate {
    /// Region ID
    pub region_id: RegionId,
    center: Point2<f32>,
    is_city: bool,
}

impl FaceTemplate {
    /// Create a new face template
    pub fn new(region_id: u8, is_city: bool) -> FaceTemplate {
        FaceTemplate {
            region_id: RegionId(region_id),
            center: Point2::new(0.0, 0.0),
            is_city,
        }
    }
}

impl noise::HasCenter for FaceTemplate {
    fn center(&self) -> Point2<f32> {
        self.center
    }
}

fn set_centers(dcel: &mut DCEL<FaceTemplate, (), Point2<f32>>) {
    let centers = dcel
        .faces()
        .map(|(id, _)| dcel.face_center(id))
        .collect::<Vec<_>>();
    for ((_, face), center) in dcel.faces_mut().zip(centers) {
        face.center = center;
    }
}

/// Map template
#[derive(Debug, Default)]
pub struct MapTemplate {
    graph: DCEL<FaceTemplate, (), Point2<f32>>,
    country_regions: HashSet<RegionId>,
    name: String,
}

impl MapTemplate {
    /// Get the graph
    pub fn graph(&self) -> &DCEL<FaceTemplate, (), Point2<f32>> {
        &self.graph
    }

    /// Get IDs of the regions available as countries
    pub fn country_regions(&self) -> &HashSet<RegionId> {
        &self.country_regions
    }

    /// Get number of countries in the map
    pub fn num_countries(&self) -> usize {
        self.country_regions.len()
    }

    /// Get name of the template
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Triangulated hexagon (6 players)
    pub fn new_triangulated_hex(radius: u8, name: String) -> MapTemplate {
        let radius = radius as i32;
        let mut map = HexMap::<FaceTemplate>::new();

        let center = Hex::new(0, 0, 0);
        map.add(center, FaceTemplate::new(0, false));

        for radius in 0..radius {
            let mut point = center + Hex::DIRECTIONS[4] * radius;
            for i in 0..6 {
                for _ in 0..radius {
                    map.add(point, FaceTemplate::new(i as u8 + 1, false));
                    point = point.neighbours()[i];
                }
            }
        }

        // add a water ring

        for radius in radius..radius + 2 {
            let mut point = center + Hex::DIRECTIONS[4] * radius;
            for i in 0..6 {
                for _ in 0..radius {
                    map.add(point, FaceTemplate::new(0, false));
                    point = point.neighbours()[i];
                }
            }
        }

        for i in 0..6 {
            let mut r = (radius - 1) / 2 + 1;
            if r % 2 == 0 {
                r = r + 1;
                if r >= radius {
                    r -= 2;
                }
            }

            let mut point = center + Hex::DIRECTIONS[i] * r;
            for _ in 0..r / 2 {
                point = point.neighbours()[(2 + i) % 6];
            }
            map.get_mut(point).unwrap().is_city = true;
        }

        let mut country_regions = HashSet::new();
        for i in 1..7 {
            country_regions.insert(RegionId(i + 1));
        }

        let mut graph = map.into_dcel(40.);
        set_centers(&mut graph);
        MapTemplate {
            graph,
            country_regions,
            name,
        }
    }

    /// Render the template
    pub fn render(&self, color_scheme: &ColorScheme) -> Document {
        let mut rng = rand::thread_rng();
        let edges = noise::noisy_edges(&self.graph, 5, 0.4, &mut rng);
        let svg = render::render_faces(&self.graph, &edges, Document::new(), |_, f| {
            (color_scheme.get_css(f.region_id.0), f.region_id.is_water())
        });
        let (x, y, w, h) = render::edges_bounding_box(&edges);
        svg.set("viewBox", (x - 20.0, y - 20.0, w + 40.0, h + 40.0))
    }

    pub fn to_gen_map(&self) -> GenMap {
        let graph = self
            .graph
            .clone()
            .map_edges(|_, _| GenEdge::new())
            .map_faces(|_, face| GenFace::new(face.region_id, face.center, face.is_city));
        GenMap::new(graph)
    }
}
