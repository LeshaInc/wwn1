//! Hexagonal map storage and conversion to DCEL

use std::collections::HashMap;
use std::{fmt, ops};

use nalgebra::{Point2, Vector2};

use super::{EdgeId, VertexId, DCEL};

/// Hex grid coordinates
///
/// Invariant: `x + y + z = 0`
#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Hex {
    /// X axis (↗)
    pub x: i32,
    /// Y axis (↖)
    pub y: i32,
    /// Z axis (↓)
    pub z: i32,
}

impl Hex {
    /// Create a new set of hex grid coordinates
    ///
    /// `x + y + z = 0` invariant is not checked
    pub const fn new(x: i32, y: i32, z: i32) -> Hex {
        Hex { x, y, z }
    }

    /// Convert axial coordinates to cube coordinates
    pub const fn from_axial(x: i32, z: i32) -> Hex {
        Hex::new(x, -x - z, z)
    }

    /// Convert cube coordinates to axial coordinates
    pub const fn to_axial(self) -> (i32, i32) {
        (self.x, self.z)
    }

    /// Convert hex coordinates to world coordinates (hexagon center)
    pub fn to_world(self, size: f32) -> Point2<f32> {
        let (q, r) = (self.x as f32, self.z as f32);
        Point2::new(
            size * (3f32.sqrt() * q + (3f32.sqrt() / 2.0) * r),
            size * 3.0 / 2.0 * r,
        )
    }

    /// Convert world coordinates to hex coordinates (rounded)
    pub fn from_world(size: f32, pos: Point2<f32>) -> Hex {
        let x = (3f32.sqrt() / 3.0 * pos.x - 1.0 / 3.0 * pos.y) / size;
        let z = (2.0 / 3.0 * pos.y) / size;
        let y = -x - z;

        let mut rx = x.round();
        let mut ry = y.round();
        let mut rz = z.round();

        let x_diff = (rx - x).abs();
        let y_diff = (ry - y).abs();
        let z_diff = (rz - z).abs();

        if x_diff > y_diff && x_diff > z_diff {
            rx = -ry - rz;
        } else if y_diff > z_diff {
            ry = -rx - rz;
        } else {
            rz = -rx - ry;
        }

        Hex::new(rx as i32, ry as i32, rz as i32)
    }

    /// Hex grid directions
    pub const DIRECTIONS: [Hex; 6] = [
        Hex::new(1, -1, 0),
        Hex::new(1, 0, -1),
        Hex::new(0, 1, -1),
        Hex::new(-1, 1, 0),
        Hex::new(-1, 0, 1),
        Hex::new(0, -1, 1),
    ];

    /// Add coordinates. You can also use the `+` operator in non-const contexts
    pub const fn add(&self, rhs: Hex) -> Hex {
        Hex::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }

    /// Subtract coordinates. You can also use the `-` operator in non-const contexts
    pub const fn sub(&self, rhs: Hex) -> Hex {
        Hex::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }

    /// Multiply every coordinate by a number. You can also use the `*` operator in non-const contexts
    pub const fn mul(&self, rhs: i32) -> Hex {
        Hex::new(self.x * rhs, self.y * rhs, self.z * rhs)
    }

    /// Get neighbouring cells
    pub const fn neighbours(&self) -> [Hex; 6] {
        [
            self.add(Hex::DIRECTIONS[0]),
            self.add(Hex::DIRECTIONS[1]),
            self.add(Hex::DIRECTIONS[2]),
            self.add(Hex::DIRECTIONS[3]),
            self.add(Hex::DIRECTIONS[4]),
            self.add(Hex::DIRECTIONS[5]),
        ]
    }

    /// Calculate distance
    pub const fn dist(&self, rhs: Hex) -> i32 {
        ((self.x - rhs.x).abs() + (self.y - rhs.y).abs() + (self.z - rhs.z).abs()) / 2
    }

    /// Get vertex coordinates
    pub fn vertex(&self, scale: f32, i: u8) -> Point2<f32> {
        let angle = -((i as f32) + 0.5) * std::f32::consts::FRAC_PI_3;
        let (sin, cos) = angle.sin_cos();
        let center = self.to_world(scale);
        center + Vector2::new(cos, sin) * scale
    }
}

impl fmt::Debug for Hex {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}, {}]", self.x, self.y, self.z)
    }
}

impl ops::Add<Hex> for Hex {
    type Output = Hex;
    fn add(self, rhs: Hex) -> Hex {
        Hex::add(&self, rhs)
    }
}

impl ops::Sub<Hex> for Hex {
    type Output = Hex;
    fn sub(self, rhs: Hex) -> Hex {
        Hex::sub(&self, rhs)
    }
}

impl ops::Mul<i32> for Hex {
    type Output = Hex;
    fn mul(self, rhs: i32) -> Hex {
        Hex::mul(&self, rhs)
    }
}

/// Hexagonal map stored as a hash map
#[derive(Debug)]
pub struct HexMap<F> {
    hexes: HashMap<Hex, F>,
}

impl<F> Default for HexMap<F> {
    fn default() -> HexMap<F> {
        HexMap {
            hexes: HashMap::new(),
        }
    }
}

impl<F> HexMap<F> {
    /// Create an empty map
    pub fn new() -> HexMap<F> {
        HexMap::default()
    }

    /// Add a hexagonal face, replacing it if it exists
    pub fn add(&mut self, hex: Hex, data: F) {
        self.hexes.insert(hex, data);
    }

    /// Get a shared reference to the face
    pub fn get(&self, hex: Hex) -> Option<&F> {
        self.hexes.get(&hex)
    }

    /// Get a mutable reference to the face
    pub fn get_mut(&mut self, hex: Hex) -> Option<&mut F> {
        self.hexes.get_mut(&hex)
    }

    /// Convert map to DCEL format with pointy topped hexagons
    pub fn into_dcel(self, scale: f32) -> DCEL<F, (), Point2<f32>> {
        let mut dcel = DCEL::new();

        let mut hex_vertices: HashMap<Hex, [VertexId; 6]> =
            HashMap::with_capacity(self.hexes.len());
        let mut hex_edges: HashMap<Hex, [EdgeId; 6]> = HashMap::with_capacity(self.hexes.len());

        for (hex, data) in self.hexes.into_iter() {
            let face = dcel.add_face(data);

            let mut p = |n: usize| {
                let a = hex_vertices
                    .get(&hex.neighbours()[n])
                    .map(|v| v[(n + 2) % 6]);
                let b = hex_vertices
                    .get(&hex.neighbours()[(n + 1) % 6])
                    .map(|v| v[(n + 4) % 6]);
                a.or(b)
                    .unwrap_or_else(|| dcel.add_vertex(hex.vertex(scale, n as u8)))
            };

            let pts = [p(0), p(1), p(2), p(3), p(4), p(5)];

            let mut e = |n: usize| {
                let hex = hex.neighbours()[n];
                let idx = (n + 3) % 6;
                hex_edges
                    .get(&hex)
                    .map(|v| v[idx])
                    .unwrap_or_else(|| dcel.add_edge(()))
            };

            let edges = [e(0), e(1), e(2), e(3), e(4), e(5)];

            dcel.connect_vertices(pts[5], pts[0], edges[0]);
            dcel.connect_vertices(pts[0], pts[1], edges[1]);
            dcel.connect_vertices(pts[1], pts[2], edges[2]);
            dcel.connect_vertices(pts[2], pts[3], edges[3]);
            dcel.connect_vertices(pts[3], pts[4], edges[4]);
            dcel.connect_vertices(pts[4], pts[5], edges[5]);
            dcel.connect_edges(edges[0], edges[1], face);
            dcel.connect_edges(edges[1], edges[2], face);
            dcel.connect_edges(edges[2], edges[3], face);
            dcel.connect_edges(edges[3], edges[4], face);
            dcel.connect_edges(edges[4], edges[5], face);
            dcel.connect_edges(edges[5], edges[0], face);

            hex_vertices.insert(hex, pts);
            hex_edges.insert(hex, edges);
        }

        dcel
    }
}
