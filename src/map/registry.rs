use serde::{Deserialize, Serialize};

use super::template::MapTemplate;

/// Map template ID
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
#[serde(transparent)]
pub struct MapTemplateId(pub usize);

/// Map template registry (stores all available templates)
#[derive(Debug, Default)]
pub struct MapTemplateRegistry {
    templates: Vec<MapTemplate>,
}

impl MapTemplateRegistry {
    /// Create an empty registry
    pub fn new() -> MapTemplateRegistry {
        Default::default()
    }

    /// Create a registry with default maps
    pub fn with_default_maps() -> MapTemplateRegistry {
        let mut registry = MapTemplateRegistry::new();
        registry.add_template(MapTemplate::new_triangulated_hex(3, "Small Hex".into()));
        registry.add_template(MapTemplate::new_triangulated_hex(5, "Medium Hex".into()));
        registry.add_template(MapTemplate::new_triangulated_hex(7, "Big Hex".into()));
        registry
    }

    /// Add a template
    pub fn add_template(&mut self, template: MapTemplate) -> MapTemplateId {
        let id = self.templates.len();
        self.templates.push(template);
        MapTemplateId(id)
    }

    /// Get the template
    pub fn get_template(&self, id: MapTemplateId) -> Option<&MapTemplate> {
        self.templates.get(id.0)
    }

    /// Iterate over the templates
    pub fn iter_templates(&self) -> impl Iterator<Item = (MapTemplateId, &MapTemplate)> + '_ {
        self.templates
            .iter()
            .enumerate()
            .map(|(i, m)| (MapTemplateId(i), m))
    }
}
