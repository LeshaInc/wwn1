use nalgebra::{Point2, Vector2};
use rand::Rng;
use std::f32::consts::{FRAC_1_SQRT_2, PI};

fn point_inside_polygon(pn: &[Point2<f32>], p: Point2<f32>) -> bool {
    let (mut i, mut j) = (0, pn.len() - 1);
    let mut inside = false;

    while i < pn.len() {
        if (pn[i].y > p.y) != (pn[j].y > p.y)
            && p.x < (pn[j].x - pn[i].x) * (p.y - pn[i].y) / (pn[j].y - pn[i].y) + pn[i].x
        {
            inside = !inside;
        }

        j = i;
        i += 1;
    }

    inside
}

fn project_point_on_line(a: Point2<f32>, b: Point2<f32>, pt: Point2<f32>) -> Point2<f32> {
    let e1 = b.coords - a.coords;
    let e2 = pt.coords - a.coords;
    let area = e1.dot(&e1);
    let val = e1.dot(&e2);
    if val > 0.0 && val < area {
        a + val * e1 / (e1.norm_squared())
    } else {
        a
    }
}

fn project_point_on_polygon(pn: &[Point2<f32>], pt: Point2<f32>) -> Point2<f32> {
    let mut proj = pt;
    let mut dist = f32::INFINITY;

    let (mut i, mut j) = (0, pn.len() - 1);
    while i < pn.len() {
        let p = project_point_on_line(pn[i], pn[j], pt);
        j = i;
        i += 1;

        let d = (p - pt).norm_squared();
        if d < dist {
            dist = d;
            proj = p;
        }
    }

    proj
}

fn sample_disc<R: Rng + ?Sized>(rng: &mut R, radius: f32) -> Point2<f32> {
    let angle = 2.0 * PI * rng.gen::<f32>();
    let distance = rng.gen_range(radius, 2.0 * radius);
    let (sin, cos) = angle.sin_cos();
    Point2::new(cos * distance, sin * distance)
}

struct Grid {
    size: f32,
    width: usize,
    height: usize,
    points: Vec<Option<Point2<f32>>>,
}

impl Grid {
    pub fn new(width: f32, height: f32, radius: f32) -> Grid {
        let size = radius * FRAC_1_SQRT_2;
        let width = (width / size).ceil() as usize;
        let height = (height / size).ceil() as usize;
        Grid {
            size,
            width,
            height,
            points: vec![None; width * height],
        }
    }

    fn get(&self, x: i64, y: i64) -> Option<Point2<f32>> {
        if x < 0 || y < 0 || (x as usize) >= self.width || (y as usize) >= self.height {
            None
        } else {
            let index = (y as usize) * self.width + (x as usize);
            self.points[index]
        }
    }

    fn point_coords(&self, pt: Point2<f32>) -> (i64, i64) {
        ((pt.x / self.size) as i64, (pt.y / self.size) as i64)
    }

    fn add(&mut self, pt: Point2<f32>) {
        let (x, y) = self.point_coords(pt);
        if x >= 0 && y >= 0 && (x as usize) < self.width && (y as usize) < self.height {
            let index = (y as usize) * self.width + (x as usize);
            self.points[index] = Some(pt);
        }
    }
}

fn valid_candidate(grid: &Grid, radius2: f32, pt: Point2<f32>) -> bool {
    let (px, py) = grid.point_coords(pt);
    for sx in -2..=2 {
        for sy in -2..=2 {
            if let Some(neighbour) = grid.get(px + sx, py + sy) {
                if (neighbour - pt).norm_squared() < radius2 {
                    return false;
                }
            }
        }
    }

    true
}

fn valid_inside_polygon(polygon: &[Point2<f32>], radius2: f32, pt: Point2<f32>) -> bool {
    if !point_inside_polygon(&polygon, pt) {
        return false;
    }

    let proj = project_point_on_polygon(&polygon, pt);
    if (proj - pt).norm_squared() < radius2 {
        return false;
    }

    true
}

/// Sample points inside polygon
pub fn sample_inside_polygon<R: Rng>(
    rng: &mut R,
    polygon: impl Iterator<Item = Point2<f32>>,
    points: &mut Vec<Point2<f32>>,
    rx: f32,
    ry: f32,
) {
    let aspect = rx / ry;
    let radius2 = rx * rx;

    let polygon = polygon
        .map(|p| Point2::new(p.x, p.y * aspect))
        .collect::<Vec<_>>();

    let init = (polygon[0], polygon[0]);
    let (min, max) = polygon
        .iter()
        .fold(init, |(inf, sup), p| (inf.inf(p), sup.sup(p)));

    let origin = Point2::new(min.x, min.y / aspect);

    let width = max.x - min.x;
    let height = max.y - min.y;

    let mut pt = Point2::new(0.0, 0.0);
    while !valid_inside_polygon(&polygon, radius2 / 2.0, pt + min.coords) {
        pt.x = rng.gen_range(0.0, width);
        pt.y = rng.gen_range(0.0, height);
    }

    let mut grid = Grid::new(width, height, rx);
    grid.add(pt);

    let mut queue = Vec::<Point2<f32>>::new();
    queue.push(pt);

    while !queue.is_empty() {
        let active_i = queue.len() - 1;
        let active = queue[active_i];
        let mut reject_active = true;

        for _ in 0..100 {
            let c = sample_disc(rng, rx) + active.coords;
            let outside = c.x < 0.0 || c.y < 0.0 || c.x >= width || c.y >= height;
            if outside
                || !valid_candidate(&grid, radius2, c)
                || !valid_inside_polygon(&polygon, radius2 / 2.0, c + min.coords)
            {
                continue;
            }

            reject_active = false;
            queue.push(c);
            grid.add(c);
        }

        if reject_active {
            points.push(origin + Vector2::new(active.x, active.y / aspect));
            queue.remove(active_i);
        }
    }
}
