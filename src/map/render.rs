//! Map rendering

use std::cell::Cell;
use std::collections::HashMap;

use nalgebra::Point2;
use rand::Rng;

use svg::node::element::path::Data;
use svg::node::element::{Definitions, Path, Pattern, Polygon, Use};
use svg::Document;

use super::{noise, EdgeId, FaceId, DCEL};

/// Convert list of points to an SVG polygon
pub fn points_to_polygon(points: &[Point2<f32>]) -> Polygon {
    Polygon::new().set(
        "points",
        points.iter().map(|p| (p.x, p.y)).collect::<Vec<_>>(),
    )
}

/// Find bounding box of the edges. Returns `(x, y, w,  h)`
pub fn edges_bounding_box(edges: &HashMap<EdgeId, Vec<Point2<f32>>>) -> (f32, f32, f32, f32) {
    let mut points = edges.values().flat_map(|v| v.iter());
    let fst = *points.next().unwrap();
    let (min, max) = points.fold((fst, fst), |(inf, sup), p| (inf.inf(p), sup.sup(p)));
    (min.x, min.y, max.x - min.x, max.y - min.y)
}

/// Renderable DCEL face
pub struct RenderableFace {
    /// Face color
    pub color: [u8; 3],
    center: Cell<Point2<f32>>,
}

impl RenderableFace {
    /// Create a new renderable face
    pub fn new(color: [u8; 3]) -> RenderableFace {
        RenderableFace {
            color,
            center: Cell::new(Point2::new(0.0, 0.0)),
        }
    }
}

impl noise::HasCenter for RenderableFace {
    fn center(&self) -> Point2<f32> {
        self.center.get()
    }
}

/// Render faces
pub fn render_faces<F, E, Col>(
    graph: &DCEL<F, E, Point2<f32>>,
    edges: &HashMap<EdgeId, Vec<Point2<f32>>>,
    mut svg: Document,
    mut get_color: Col,
) -> Document
where
    Col: FnMut(FaceId, &F) -> (String, bool),
{
    svg = svg.add(
        Pattern::new()
            .set("id", "water")
            .set("x", 0)
            .set("y", 0)
            .set("width", 32.5)
            .set("height", 4.0)
            .set("patternUnits", "userSpaceOnUse")
            .add(
                Path::new()
                    .set("d", include_str!("../../assets/water.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.15)"),
            ),
    );

    for i in 0..=1 {
        for (face_id, face) in graph.faces() {
            let points = noise::face_noisy_points(&graph, &edges, face_id);
            let mut poly = points_to_polygon(&points);
            let (col, is_water) = get_color(face_id, face);

            poly = poly.set("fill", col.as_str());

            if i == 0 {
                poly = poly.set("stroke", col).set("stroke-width", 1.5);
            }

            if is_water && i == 1 {
                svg = svg.add(poly.clone());
                svg = svg.add(poly.set("fill", "url(#water)"));
            } else {
                svg = svg.add(poly);
            }
        }
    }

    for (face_id, _) in graph.faces() {
        let points = noise::face_noisy_points(&graph, &edges, face_id);
        let poly = points_to_polygon(&points);
        svg = svg.add(
            poly.set("fill", "transparent")
                .set("stroke", "rgba(0, 0, 0, 0.07)")
                .set("stroke-width", 1.5),
        );
    }

    svg
}

/// Render a map to SVG
pub fn render<E>(dcel: &DCEL<RenderableFace, E, Point2<f32>>) -> Document {
    let mut svg = Document::new().set("xmlns:xlink", "http://www.w3.org/1999/xlink");

    svg = svg.add(
        Definitions::new()
            .add(
                Path::new()
                    .set("id", "tree0")
                    .set("d", include_str!("../../assets/tree0.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.5)"),
            )
            .add(
                Path::new()
                    .set("id", "tree1")
                    .set("d", include_str!("../../assets/tree1.txt"))
                    .set("fill", "rgba(0, 0, 0, 0.5)"),
            ),
    );

    for (face_id, face) in dcel.faces() {
        face.center.set(dcel.face_center(face_id));
    }

    let mut rng = rand::thread_rng();

    let edges = noise::noisy_edges(dcel, 5, 0.4, &mut rng);

    let mut tree0 = Vec::new();
    let mut tree1 = Vec::new();

    for (face_id, face) in dcel.faces() {
        let points = noise::face_noisy_points(dcel, &edges, face_id);
        let iter = points.iter().copied();

        if rng.gen() {
            super::sample_inside_polygon(&mut rng, iter, &mut tree0, 10.0, 17.0);
        } else {
            super::sample_inside_polygon(&mut rng, iter, &mut tree1, 9.0, 16.0);
        }

        let [r, g, b] = face.color;
        let poly = points_to_polygon(&points);
        svg = svg.add(poly.set("fill", format!("rgb({}, {}, {})", r, g, b)));
    }

    for (edge_id, _) in dcel.edges() {
        let points = &edges[&edge_id];
        let mut data = Data::new().move_to((points[0].x, points[0].y));
        for pt in &points[1..] {
            data = data.line_to((pt.x, pt.y));
        }

        svg = svg.add(
            Path::new()
                .set("d", data)
                .set("stroke", "black")
                .set("fill", "none")
                .set("stroke-width", 1),
        );
    }

    for point in tree0 {
        svg = svg.add(
            Use::new()
                .set("xlink:href", "#tree0")
                .set("x", point.x)
                .set("y", point.y),
        );
    }

    for point in tree1 {
        svg = svg.add(
            Use::new()
                .set("xlink:href", "#tree1")
                .set("x", point.x)
                .set("y", point.y),
        );
    }

    let (x, y, w, h) = edges_bounding_box(&edges);
    svg.set("viewBox", (x - 20.0, y - 20.0, w + 40.0, h + 40.0))
}
