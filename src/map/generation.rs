use nalgebra::Point2;
use rand::distributions::{Distribution, WeightedIndex};
use svg::node::element::Use;
use svg::Document;

use super::template::RegionId;
use super::{noise, render, ColorScheme, Terrain, DCEL};

/// Generation map face
pub struct GenFace {
    region_id: RegionId,
    center: Point2<f32>,
    terrain: Terrain,
}

impl noise::HasCenter for GenFace {
    fn center(&self) -> Point2<f32> {
        self.center
    }
}

impl GenFace {
    /// Create a new face
    pub fn new(region_id: RegionId, center: Point2<f32>, is_city: bool) -> GenFace {
        GenFace {
            region_id,
            center,
            terrain: if is_city {
                Terrain::Urban
            } else if region_id.is_water() {
                Terrain::Water
            } else {
                Terrain::Forest
            },
        }
    }
}

pub struct GenEdge {
    // TODO: rivers
}

impl GenEdge {
    pub fn new() -> GenEdge {
        GenEdge {}
    }
}

/// Map representation during world generation
pub struct GenMap {
    graph: DCEL<GenFace, GenEdge, Point2<f32>>,
}

impl GenMap {
    /// Create a new generaton map
    pub fn new(graph: DCEL<GenFace, GenEdge, Point2<f32>>) -> GenMap {
        GenMap { graph }
    }

    /// Get the graph
    pub fn graph(&self) -> &DCEL<GenFace, GenEdge, Point2<f32>> {
        &self.graph
    }

    pub fn gen_terrain(&mut self) {
        let mut rng = rand::thread_rng();
        let (weights, choices) = Terrain::random_weights();
        let weights = WeightedIndex::new(weights).unwrap();

        for (_, face) in self.graph.faces_mut() {
            if face.terrain == Terrain::Water || face.terrain == Terrain::Urban {
                continue;
            }

            let idx = weights.sample(&mut rng);
            face.terrain = choices[idx];
        }
    }

    /// Render the map
    pub fn render(&self, color_scheme: &ColorScheme) -> Document {
        let mut rng = rand::thread_rng();
        let edges = noise::noisy_edges(&self.graph, 5, 0.4, &mut rng);

        let mut svg = Document::new().set("xmlns:xlink", "http://www.w3.org/1999/xlink");

        svg = svg.add(Terrain::svg_defs());

        svg = render::render_faces(&self.graph, &edges, svg, |_, f| {
            (
                color_scheme.get_css(f.region_id.0),
                f.terrain == Terrain::Water,
            )
        });

        let mut sampled_points = Vec::new();
        for (face_id, face) in self.graph.faces() {
            let pattern = match face.terrain.sampled_pattern(&mut rng) {
                Some(v) => v,
                None => continue,
            };

            let points = noise::face_noisy_points(&self.graph, &edges, face_id);
            let iter = points.iter().copied();

            sampled_points.clear();
            super::sample_inside_polygon(
                &mut rng,
                iter,
                &mut sampled_points,
                pattern.rx,
                pattern.ry,
            );

            for point in &sampled_points {
                svg = svg.add(
                    Use::new()
                        .set("xlink:href", pattern.name)
                        .set("x", point.x)
                        .set("y", point.y),
                );
            }
        }

        let (x, y, w, h) = render::edges_bounding_box(&edges);
        svg.set("viewBox", (x - 20.0, y - 20.0, w + 40.0, h + 40.0))
    }
}
