//! Map representation

mod hex;
pub use self::hex::{Hex, HexMap};

mod dcel;
pub use self::dcel::{EdgeId, FaceId, VertexId, DCEL};

mod sampling;
pub use self::sampling::sample_inside_polygon;

mod terrain;
pub use self::terrain::Terrain;

mod colorscheme;
pub use self::colorscheme::ColorScheme;

mod template;
pub use self::template::{FaceTemplate, MapTemplate, RegionId};

mod generation;
pub use self::generation::{GenEdge, GenFace, GenMap};

mod registry;
pub use self::registry::{MapTemplateId, MapTemplateRegistry};

pub mod noise;
pub mod render;
