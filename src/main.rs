use tokio::runtime::Runtime;

use wwn1::server::Server;

fn main() {
    tracing_subscriber::fmt::init();

    let rt = Runtime::new().expect("Cannot initialize tokio runtime");
    rt.block_on(async {
        let server = Server::new("0.0.0.0:8000")
            .await
            .expect("Cannot create server");
        server.run().await;
    });
}
