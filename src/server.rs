//! Server

use std::net::SocketAddr;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

use anyhow::{Context, Result};
use slab::Slab;
use tokio::net::{lookup_host, TcpListener, TcpStream, ToSocketAddrs};
use tokio::sync::RwLock;
use tokio::time::Duration;
use tracing::{error, info, instrument};

use crate::client::{Client, ClientContext, ClientId};
use crate::map::{ColorScheme, MapTemplateRegistry};
use crate::notification::ValueNotificationSender;
use crate::protocol::*;

/// Server
pub struct Server {
    listener: TcpListener,
    context: Arc<ServerContext>,
}

pub struct ServerContext {
    num_clients: AtomicUsize,
    num_clients_notification: ValueNotificationSender<Message>,
    pub clients: RwLock<Slab<Arc<ClientContext>>>,
    pub map_registry: MapTemplateRegistry,
    pub list_maps_message: String,
}

impl ServerContext {
    pub fn new() -> ServerContext {
        let color_scheme = ColorScheme::open("assets/colorscheme.txt").unwrap();
        let map_registry = MapTemplateRegistry::with_default_maps();
        let maps = map_registry
            .iter_templates()
            .map(|(_, template)| MapEntry {
                name: template.name().to_owned(),
                num_countries: template.num_countries() as _,
                svg: template.render(&color_scheme).to_string(),
            })
            .collect();
        let list_maps_message = serde_json::to_string(&Message::from(ResMapList { maps })).unwrap();

        ServerContext {
            num_clients: AtomicUsize::new(0),
            num_clients_notification: ValueNotificationSender::new(
                Duration::from_millis(300),
                NotificationNumClients { num_clients: 0 }.into(),
            ),
            clients: RwLock::new(Slab::new()),
            map_registry,
            list_maps_message,
        }
    }

    pub async fn add_client(&self, ctx: Arc<ClientContext>) -> ClientId {
        let num_clients = self.num_clients.fetch_add(1, Ordering::Relaxed) + 1;
        self.num_clients_notification
            .update(NotificationNumClients { num_clients }.into())
            .await;

        self.num_clients_notification
            .subscribe(ctx.tx.clone())
            .await;

        let mut lock = self.clients.write().await;
        let id = ClientId(lock.insert(ctx));

        id
    }

    pub async fn remove_client(&self, id: ClientId) {
        let num_clients = self.num_clients.fetch_sub(1, Ordering::Relaxed) - 1;
        self.num_clients_notification
            .update(NotificationNumClients { num_clients }.into())
            .await;

        let mut lock = self.clients.write().await;
        lock.remove(id.0);
    }
}

impl Server {
    /// Construct a server bound to the specified address
    pub async fn new<A: ToSocketAddrs>(addrs: A) -> Result<Server> {
        let addrs = lookup_host(addrs).await.context("Cannot lookup host")?;
        let addrs = addrs.collect::<Vec<_>>();
        let listener = TcpListener::bind(&addrs[..])
            .await
            .with_context(|| format!("Cannot listen on {:?}", addrs))?;

        info!(?addrs, "Server started");

        Ok(Server {
            listener,
            context: Arc::new(ServerContext::new()),
        })
    }

    /// Run the server
    pub async fn run(mut self) -> ! {
        let ctx = self.context.clone();
        tokio::spawn(async move {
            ctx.num_clients_notification.run_loop().await;
        });

        loop {
            if let Err(error) = self.accept_client().await {
                error!(?error);
            }
        }
    }

    async fn accept_client(&mut self) -> Result<()> {
        let res = self.listener.accept().await;
        let (stream, addr) = res.context("Error while accepting TCP connection")?;

        let ctx = self.context.clone();

        tokio::spawn(async move {
            let _ = client(ctx, stream, addr).await;
        });

        Ok(())
    }
}

#[instrument(err, skip(ctx, stream))]
async fn client(ctx: Arc<ServerContext>, stream: TcpStream, addr: SocketAddr) -> Result<()> {
    info!("Accepting connection");

    let stream = tokio_tungstenite::accept_async(stream)
        .await
        .context("Unable to perform websocket handshake")?;

    let mut client = Client::new(stream, ctx).await;
    client.handle().await?;
    client.destroy().await;

    info!("Disconnecting");

    Ok(())
}
