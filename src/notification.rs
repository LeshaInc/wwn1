use std::collections::{HashMap, HashSet};

use crossbeam_queue::SegQueue;
use slab::Slab;
use tokio::sync::mpsc::UnboundedSender;
use tokio::sync::{Notify, RwLock};
use tokio::time::{self, Duration, Instant};
use tracing::error;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct SubscriptionId(usize);

/// Sends notification events to subscribers
pub struct ValueNotificationSender<M> {
    delay: Duration,
    notify: Notify,
    message: RwLock<M>,
    receivers: RwLock<Slab<(Instant, UnboundedSender<M>)>>,
    to_unsubscribe: SegQueue<usize>,
}

impl<M: Clone> ValueNotificationSender<M> {
    /// Create a new notification sender
    ///
    /// If more than one updates happen during `delay`, only the last message will be sent
    pub fn new(delay: Duration, init: M) -> Self {
        ValueNotificationSender {
            delay,
            notify: Notify::new(),
            message: RwLock::new(init),
            receivers: RwLock::new(Slab::new()),
            to_unsubscribe: SegQueue::new(),
        }
    }

    async fn clone_message(&self) -> M {
        self.message.read().await.clone()
    }

    /// Subscribe to notifications
    pub async fn subscribe(&self, chan: UnboundedSender<M>) -> Option<SubscriptionId> {
        let message = self.clone_message().await;
        if let Err(_) = chan.send(message) {
            return None;
        }

        let mut slab = self.receivers.write().await;
        let idx = slab.insert((Instant::now(), chan));
        Some(SubscriptionId(idx))
    }

    /// Unsubscribe from notifications. If the receiving part of the sender becomes closed, the
    /// client will be unsubscribed automatically
    pub async fn unsubscribe(&self, id: SubscriptionId) {
        self.to_unsubscribe.push(id.0);
    }

    /// Send an update
    pub async fn update(&self, message: M) {
        {
            let mut lock = self.message.write().await;
            *lock = message;
        }

        self.notify.notify_one();
    }

    /// Process updates in an infinite loop
    pub async fn run_loop(&self) {
        let mut to_update = HashSet::new();
        let mut updated = Vec::new();
        let mut update_times = HashMap::new();
        let mut update_deadline = None;

        fn set_deadline(a: &mut Option<Instant>, b: Instant) {
            *a = match *a {
                Some(v) if v > b => Some(b),
                None => Some(b),
                v => v,
            };
        }

        loop {
            let is_notified = match update_deadline {
                Some(deadline) => {
                    tokio::select! {
                        _ = self.notify.notified() => true,
                        _ = time::sleep_until(deadline) => false,
                    }
                }
                None => {
                    self.notify.notified().await;
                    true
                }
            };

            let now = Instant::now();

            let slab = self.receivers.read().await;
            let message = self.clone_message().await;

            if is_notified {
                to_update.clear();
            }

            update_deadline = None;

            for &id in &to_update {
                let update_time = match update_times.get_mut(&id) {
                    Some(v) => v,
                    None => {
                        error!("cannot retrieve subscriber update time; skipping update");
                        updated.push(id);
                        continue;
                    }
                };

                if now < *update_time + self.delay {
                    set_deadline(&mut update_deadline, *update_time + self.delay);
                    continue;
                }

                if let Err(_) = slab[id].1.send(message.clone()) {
                    self.to_unsubscribe.push(id);
                    continue;
                }

                *update_time = now;
                updated.push(id);
            }

            for id in updated.drain(..) {
                to_update.remove(&id);
            }

            for (id, (creation_time, receiver)) in slab.iter().filter(|_| is_notified) {
                let update_time = update_times.entry(id).or_insert(*creation_time);

                if now < *update_time + self.delay {
                    to_update.insert(id);
                    set_deadline(&mut update_deadline, *update_time + self.delay);
                } else {
                    *update_time = now;
                    if let Err(_) = receiver.send(message.clone()) {
                        self.to_unsubscribe.push(id);
                        continue;
                    }
                }
            }

            drop(slab);

            if self.to_unsubscribe.is_empty() {
                continue;
            }

            let mut slab = self.receivers.write().await;
            while let Some(id) = self.to_unsubscribe.pop() {
                slab.remove(id);
                to_update.remove(&id);
                update_times.remove(&id);
            }
        }
    }
}
